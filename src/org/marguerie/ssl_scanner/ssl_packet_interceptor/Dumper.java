package org.marguerie.ssl_scanner.ssl_packet_interceptor;

import org.marguerie.ssl_scanner.SSLPacketParser.protocols.AlertProtocol;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.ApplicationProtocol;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.ChangeCipherSpecProtocol;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.HandshakeProtocol;

public class Dumper extends GenericInterceptor implements IInterceptor {

	@Override
	public void handleAlert(AlertProtocol packet) {
		super.handleAlert(packet);

		// TODO Auto-generated method stub
		System.out.println(packet);
	}

	@Override
	public void handleHandshake(HandshakeProtocol packet) {
		super.handleHandshake(packet);

		// TODO Auto-generated method stub
		System.out.println(packet);
	}

	@Override
	public void handleChangeCipherSpec(ChangeCipherSpecProtocol packet) {
		super.handleChangeCipherSpec(packet);

		// TODO Auto-generated method stub
		System.out.println(packet);
	}

	@Override
	public void handleApplication(ApplicationProtocol packet) {
		super.handleApplication(packet);

		// TODO Auto-generated method stub
		System.out.println(packet);
	}

}
