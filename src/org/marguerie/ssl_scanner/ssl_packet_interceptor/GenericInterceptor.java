package org.marguerie.ssl_scanner.ssl_packet_interceptor;

import java.util.ArrayList;

import org.jnetpcap.packet.JFlow;
import org.jnetpcap.packet.PcapPacket;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.AlertProtocol;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.ApplicationProtocol;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.ChangeCipherSpecProtocol;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.HandshakeProtocol;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.ProtocolMessage;

public class GenericInterceptor implements IInterceptor {
	private JFlow mFlow = null;
	private ArrayList<ProtocolMessage> mMessages = new ArrayList<>();
	private boolean mEncryptedMessages = false;
	
	@Override
	public void addPacket(PcapPacket packet) {
		if (mFlow == null) {
			mFlow = new JFlow(packet.getState().getFlowKey());
		}
		mFlow.add(packet);
	}

	@Override
	public void handleAlert(AlertProtocol packet) {
		mMessages.add(packet);
	}

	@Override
	public void handleHandshake(HandshakeProtocol packet) {
		if (mEncryptedMessages)
		{
			packet.convertToEncrypted();
		}
		mMessages.add(packet);
	}

	@Override
	public void handleChangeCipherSpec(ChangeCipherSpecProtocol packet) {
		if (packet.getCCS() == 1)
			mEncryptedMessages = true;
		mMessages.add(packet);
	}

	@Override
	public void handleApplication(ApplicationProtocol packet) {
		mMessages.add(packet);
	}

}
