package org.marguerie.ssl_scanner.ssl_packet_interceptor.sources;

import org.jnetpcap.Pcap;

public class FileSource implements ISource {

	private String mFilename = null;
	
	public FileSource(String filename)
	{
		mFilename = filename;
	}
	
	@Override
	public Pcap getPcap() {
	    StringBuilder errbuf = new StringBuilder();
	    return Pcap.openOffline(mFilename, errbuf);
	}

}
