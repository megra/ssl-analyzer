package org.marguerie.ssl_scanner.ssl_packet_interceptor.sources;

import org.jnetpcap.Pcap;

public interface ISource {
	public Pcap getPcap();
}
