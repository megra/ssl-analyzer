package org.marguerie.ssl_scanner.ssl_packet_interceptor.sources;

import java.util.ArrayList;
import java.util.List;

import org.jnetpcap.Pcap;
import org.jnetpcap.PcapIf;

public class LiveSource implements ISource {

	@Override
	public Pcap getPcap() {
		StringBuilder errbuf = new StringBuilder(); // For any error msgs

		/***************************************************************************
		 * First: we open up the selected device
		 **************************************************************************/
		PcapIf device = getIfAny();
		if (device == null)
		{
			return null;
		}

		/***************************************************************************
		 * Second: we open up the selected device
		 **************************************************************************/
		int snaplen = 64 * 1024;           // Capture all packets, no trucation
		int flags = Pcap.MODE_PROMISCUOUS; // capture all packets
		int timeout = 10 * 1000;           // 10 seconds in millis
		Pcap pcap = Pcap.openLive(device.getName(), snaplen, flags, timeout, errbuf);

		if (pcap == null) {
			System.err.printf("Error while opening device for capture: " + errbuf.toString());
			return null;
		}
		
		return pcap;
	}
	
	public static PcapIf getIfAny()
	{
		List<PcapIf> alldevs = new ArrayList<PcapIf>(); // Will be filled with NICs
		StringBuilder errbuf = new StringBuilder(); // For any error msgs

		/***************************************************************************
		 * First: get a list of devices on this system
		 **************************************************************************/
		int r = Pcap.findAllDevs(alldevs, errbuf);
		if (r == Pcap.NOT_OK || alldevs.isEmpty()) {
			System.err.printf("Can't read list of devices, error is %s", errbuf.toString());
			return null;
		}

		/***************************************************************************
		 * Second: return the "any" device if found
		 **************************************************************************/
		for (PcapIf device : alldevs) {
			if (device.getName().equals("any")) {
				return device;
			}
		}

		/***************************************************************************
		 * Third: return any device if "any" not found
		 **************************************************************************/
		PcapIf device = alldevs.get(0);
		String deviceDesc = (device.getDescription() != null) ? device.getDescription() : device.getName();
		System.out.printf("Choosing '%s' on your behalf: ", deviceDesc);
		return device;
	}


}
