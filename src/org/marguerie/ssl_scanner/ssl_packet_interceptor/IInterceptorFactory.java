package org.marguerie.ssl_scanner.ssl_packet_interceptor;

public interface IInterceptorFactory {
	public IInterceptor create();
}
