package org.marguerie.ssl_scanner.ssl_packet_interceptor;

public class DumperFactory implements IInterceptorFactory {

	@Override
	public IInterceptor create() {
		return new Dumper();
	}

}
