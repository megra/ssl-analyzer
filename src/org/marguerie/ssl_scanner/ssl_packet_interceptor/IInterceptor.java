package org.marguerie.ssl_scanner.ssl_packet_interceptor;

import org.jnetpcap.packet.PcapPacket;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.AlertProtocol;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.ApplicationProtocol;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.ChangeCipherSpecProtocol;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.HandshakeProtocol;

public interface IInterceptor {

	void addPacket(PcapPacket packet);

	void handleAlert(AlertProtocol packet);

	void handleHandshake(HandshakeProtocol packet);

	void handleChangeCipherSpec(ChangeCipherSpecProtocol packet);

	void handleApplication(ApplicationProtocol packet);

}
