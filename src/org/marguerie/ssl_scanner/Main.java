package org.marguerie.ssl_scanner;

import joptsimple.OptionParser;
import joptsimple.OptionSet;

import org.jnetpcap.Pcap;
import org.jnetpcap.PcapBpfProgram;
import org.jnetpcap.packet.PcapPacketHandler;
import org.marguerie.ssl_scanner.ssl_packet_interceptor.DumperFactory;
import org.marguerie.ssl_scanner.ssl_packet_interceptor.sources.FileSource;
import org.marguerie.ssl_scanner.ssl_packet_interceptor.sources.ISource;
import org.marguerie.ssl_scanner.ssl_packet_interceptor.sources.LiveSource;

public class Main {

	/**
	 * Constants.
	 */
	private static final int SOURCE_LIVE	= 1;
	private static final int SOURCE_FILE	= 2;
	private static final int SOURCE_DEFAULT	= SOURCE_LIVE;

	/**
	 * Main startup method
	 * 
	 * @param args
	 *          ignored
	 */
	public static void main(String[] args) {
		// options
		int source = SOURCE_DEFAULT;
		String source_filename = null;
		
		// parse options
		try {
	        OptionParser parser = new OptionParser("hf:l");
	        OptionSet options = parser.parse(args);
	
	        if (options.has("h"))
	        	displayUsage(0);
	        if (options.has("l"))
	        	source = SOURCE_LIVE;
	        if (options.has("f")) {
	        	if (options.hasArgument("f")) {
		        	source = SOURCE_FILE;
		        	source_filename = (String) options.valueOf("f");
	        	}
	        	else {
	        		displayUsage(1);
	        	}
	        }
		}
		catch (Exception e) {
			e.printStackTrace();
			displayUsage(2);
		}
        
		// act on options
		ISource packetsSource;
		if (source == SOURCE_FILE)
			packetsSource = new FileSource(source_filename);
		else
			packetsSource = new LiveSource();
		
		// start
		start(packetsSource);
	}
	
	private static void displayUsage(int status) {
		// TODO Auto-generated method stub
		System.out.println("Usage: ssl_scanner [-h] [-l] [-f FILENAME]");
		System.out.println("Intercepts HTTPS packets and analyze it to find weak cryptographic implementations.");
		System.out.println();
		System.out.println("Options:");
		System.out.println("\t-h\t\tdisplay this message");
		System.out.println("\t-l\t\tuse live packet capture");
		System.out.println("\t-f FILENAME\tuse file capture from FILENAME");
		System.exit(status);
	}

	private static void start(ISource packetsSource)
	{
		Pcap pcap = packetsSource.getPcap();
		
		// Check
		if (pcap == null) {
			System.err.printf("Error: no capture interface!");
			return;
		}

		// Setup a packet filter
		if (!setupFilter(pcap, "tcp port 443")) {
			System.err.println("Couldn't setup filter!");
			return;
		}
		
		// Start receiving packets
		System.out.println("Listening to packets:");
		PcapPacketHandler<String> packetHandler = new SSLPacketDecoder(new DumperFactory());
		int r = 1;
		while (r != -2 && r != 0) {
			r = pcap.loop(Pcap.LOOP_INFINITE, packetHandler, "ssl scanner");
		}
		
		// Clean
		pcap.close();
	}


	private static boolean setupFilter(Pcap pcap, String expression) {
		PcapBpfProgram program = new PcapBpfProgram();
		int optimize = 0;         // 0 = false  
		int netmask = 0xFFFFFF00; // 255.255.255.0  

		if (pcap.compile(program, expression, optimize, netmask) != Pcap.OK) {  
			System.err.println(pcap.getErr());  
			return false;
		}  

		if (pcap.setFilter(program) != Pcap.OK) {  
			System.err.println(pcap.getErr());  
			return false;         
		}

		return true;
	}
}
