package org.marguerie.ssl_scanner;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;

import org.jnetpcap.nio.JBuffer;
import org.jnetpcap.packet.JFlowKey;
import org.jnetpcap.packet.Payload;
import org.jnetpcap.packet.PcapPacket;
import org.jnetpcap.packet.PcapPacketHandler;
import org.jnetpcap.protocol.network.Ip4;
import org.jnetpcap.protocol.tcpip.Tcp;
import org.marguerie.ssl_scanner.SSLPacketParser.SSLPacketParser;
import org.marguerie.ssl_scanner.SSLPacketParser.exception.MissingDataException;
import org.marguerie.ssl_scanner.SSLPacketParser.exception.ParserException;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.AlertProtocol;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.ApplicationProtocol;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.ChangeCipherSpecProtocol;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.HandshakeProtocol;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.ProtocolMessage;
import org.marguerie.ssl_scanner.ssl_packet_interceptor.IInterceptor;
import org.marguerie.ssl_scanner.ssl_packet_interceptor.IInterceptorFactory;


/**
 * @author megra
 * This class decodes TCP packet's payload according 
 */
public class SSLPacketDecoder implements PcapPacketHandler<String> {
	
	/**
	 * Stores a history of the flows
	 */
	HashMap<JFlowKey, IInterceptor> mFlows = new HashMap<>();
	HashMap<JFlowKey, byte[]> mReassemblePreviousTCPSegment = new HashMap<>();
	
	/**
	 * SSL packet analyzer factory
	 */
	IInterceptorFactory mInterceptorFactory = null;

	/**
	 * Ctor.
	 * @param interceptorFactory The factory to create SSL packet analyzer
	 */
	public SSLPacketDecoder(IInterceptorFactory interceptorFactory)
	{
		mInterceptorFactory = interceptorFactory;
	}


	/** 
	 * @param ip
	 * @return
	 */
	public String ipIntToString(int ip)
	{
		byte[] bytes = BigInteger.valueOf(ip).toByteArray();
		InetAddress address;
		try {
			address = InetAddress.getByAddress(bytes);
			return address.getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
			return "null";
		}
	}

	private byte[] mergeByteArrays(byte[] a, byte[] b)
	{
		byte[] combined = new byte[a.length + b.length];
		System.arraycopy(a, 0, combined, 0,			a.length);
		System.arraycopy(b, 0, combined, a.length,	b.length);
		return combined;
	}
	
	
	@Override
	public void nextPacket(PcapPacket packet, String user) {
		// TCP/IP packet
		if (packet.hasHeader(Ip4.ID) && packet.hasHeader(Tcp.ID))
		{
			//
			// Store packet in JFlow
			//
			
			// New connection
			JFlowKey flowKey = packet.getState().getFlowKey();
			if (!mFlows.containsKey(flowKey))
			{
				mFlows.put(flowKey, mInterceptorFactory.create());
				
//				Ip4 ip = packet.getHeader(new Ip4());
//				Tcp tcp = packet.getHeader(new Tcp());
//				System.out.println("TCP packet: src_port=" + tcp.source()
//						+ ", dst_port=" + tcp.destination()
//						+ ", src=" + ipIntToString(ip.sourceToInt())
//						+ ", dst=" + ipIntToString(ip.destinationToInt())
//						+ ", ip_id=" + ip.id());
			}

			// Check if the packet has a payload or is just a SYN/SYN-ACK/ACK/FIN/FIN-ACK/ACK/...
			Payload payload = packet.getHeader(new Payload());
			if (payload == null) {
				return;
			}
			
			// Debug
			// System.out.println(packet.getHeader(new Tcp()));
			// System.out.println();
						
			// Extract payload
			JBuffer buf;
			if (mReassemblePreviousTCPSegment.containsKey(packet.getState().getFlowKey()))
			{
				byte[] old_payload = mReassemblePreviousTCPSegment.get(packet.getState().getFlowKey());
				byte[] new_payload = mergeByteArrays(old_payload, payload.data());
				buf = new JBuffer(new_payload);
			}
			else
			{
				buf = payload;
			}
			mReassemblePreviousTCPSegment.remove(packet.getState().getFlowKey());
			
			
			SSLPacketParser sslParser = new SSLPacketParser();
			
			// Try to parse messages, reassemble next packet if needed
			try {
				sslParser.parse(buf, 0);
			} catch (MissingDataException e) {
				mReassemblePreviousTCPSegment.put(packet.getState().getFlowKey(), buf.getByteArray(0, buf.size()));
				return;
			} catch (ParserException e) {
				System.out.println("[Invalid SSL packet!]");
			}
			
			// Add packets
			if (sslParser.getSSLPackets().size() != 0) {
				for (ProtocolMessage sslpacket : sslParser.getSSLPackets()) {
					if (sslpacket instanceof AlertProtocol)
						mFlows.get(packet.getState().getFlowKey()).handleAlert((AlertProtocol) sslpacket);
					else if (sslpacket instanceof HandshakeProtocol)
						mFlows.get(packet.getState().getFlowKey()).handleHandshake((HandshakeProtocol) sslpacket);
					else if (sslpacket instanceof ChangeCipherSpecProtocol)
						mFlows.get(packet.getState().getFlowKey()).handleChangeCipherSpec((ChangeCipherSpecProtocol) sslpacket);
					else if (sslpacket instanceof ApplicationProtocol)
						mFlows.get(packet.getState().getFlowKey()).handleApplication((ApplicationProtocol) sslpacket);
					else
						System.out.println("Unexpected protocole meessage type received!");
					
					// TODO: dispatch messages to SSL analyzer
				}
			}
		}
	}
}
