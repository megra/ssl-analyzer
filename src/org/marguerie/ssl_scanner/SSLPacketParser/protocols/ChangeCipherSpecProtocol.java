package org.marguerie.ssl_scanner.SSLPacketParser.protocols;

public class ChangeCipherSpecProtocol extends ProtocolMessage {
	
	public static final int CCS_PROTOCOL_TYPE_NULL = -1;
	
	private int mCCSProtocolType = -1;
	
	public String toString()
	{
		String ret = "[ChangeCipherSuiteSecProtocoleMessage]\n";
		ret += super.toString();
		ret += "CCS protocol type: " + mCCSProtocolType + "\n";
		
		return ret;
	}
	
	public void setCCS(int v)
	{
		mCCSProtocolType = v;
	}
	
	public int getCCS()
	{
		return mCCSProtocolType;
	}
}
