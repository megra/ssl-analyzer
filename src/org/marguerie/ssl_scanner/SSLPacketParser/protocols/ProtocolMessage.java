package org.marguerie.ssl_scanner.SSLPacketParser.protocols;

import org.marguerie.ssl_scanner.SSLPacketParser.exception.UnsupportedSSLVersionException;

public abstract class ProtocolMessage {
	/**
	 * Constants
	 */
	public final static int CONTENTTYPE_NULL			= -1;
	public final static int CONTENTTYPE_CHANGECYPHER	= 0x14;
	public final static int CONTENTTYPE_ALERT			= 0x15;
	public final static int CONTENTTYPE_HANDSHAKE		= 0x16;
	public final static int CONTENTTYPE_APPLICATION		= 0x17; 
	
	public final static int MAJORVERSION_NULL			= -1;
	public final static int MINORVERSION_NULL			= -1;

	/**
	 * Internal state
	 */
	public Integer mMajorVersion	= -1;
	public Integer mMinorVersion	= -1;
	
	public void setVersion(int major, int minor) throws UnsupportedSSLVersionException {
		if (major != 3 || minor < 0 || minor > 3)
		{
			throw new UnsupportedSSLVersionException("Unsupported version: " + major + "." + minor);
		}

		mMajorVersion = new Integer(major);
		mMinorVersion = new Integer(minor);
	}
	
	public String versionToString()
	{
		if (mMajorVersion == 3)
		{
			switch (mMinorVersion)
			{
			case 0:
				return "SSL v3";
			case 1:
				return "TLS 1.0";
			case 2:
				return "TLS 1.1";
			case 3:
				return "TLS 1.2";
			}
		}

		return "unknown";
	}
	
	public String toString()
	{
		String ret = "";
		
		ret += "Version: " + mMajorVersion + "." + mMinorVersion;
		ret += " (" + versionToString() + ")\n";
		
		return ret;
	}
	
}
