package org.marguerie.ssl_scanner.SSLPacketParser.protocols.handshake_messages;

/**
 * @author megra
 *
 */
public abstract class HandshakeMessage {

	/**
	 * Constants.
	 */
	public static final byte TYPE_HELLO_REQUEST 		= 0;
	public static final byte TYPE_CLIENT_HELLO			= 1;
	public static final byte TYPE_SERVER_HELLO			= 2;
	public static final byte TYPE_NEW_SESSION_TICKET	= 4;
	public static final byte TYPE_CERTIFICATE			= 11;
	public static final byte TYPE_SERVER_KEY_EXCHANGE	= 12;
	public static final byte TYPE_CERTIFICATE_REQUEST	= 13;
	public static final byte TYPE_SERVER_HELLO_DONE		= 14;
	public static final byte TYPE_CERTIFICATE_VERIFY	= 15;
	public static final byte TYPE_CLIENT_KEY_EXCHANGE	= 16;
	public static final byte TYPE_FINISHED				= 20;
	
}
