package org.marguerie.ssl_scanner.SSLPacketParser.protocols;

public class ApplicationProtocol extends ProtocolMessage {

	private byte[] mData = new byte[0];
	
	public void setData(byte[] data) {
		mData = data;
	}
	
	public byte[] getData()
	{
		return mData;
	}
	
	public String toString()
	{
		String ret = "[ApplicationProtocolMessage]\n";
		ret += super.toString();
		
		ret += "Length of data: " + mData.length + "\n";
		// ret += "Data: " + (new String(mData)) + "\n";
		
		return ret;
	}

}
