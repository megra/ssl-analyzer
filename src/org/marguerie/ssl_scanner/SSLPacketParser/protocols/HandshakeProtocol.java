package org.marguerie.ssl_scanner.SSLPacketParser.protocols;

import java.util.ArrayList;

import org.marguerie.ssl_scanner.SSLPacketParser.protocols.handshake_messages.EncryptedMessage;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.handshake_messages.HandshakeMessage;

public class HandshakeProtocol extends ProtocolMessage {
	private ArrayList<HandshakeMessage> mHandshakeMessages = new ArrayList<>();

	public void addMessage(HandshakeMessage m) {
		mHandshakeMessages.add(m);
	}
	
	public ArrayList<HandshakeMessage> getMessages()
	{
		return mHandshakeMessages;
	}
	
	public String toString()
	{
		String ret = "[HandshakeProtocole]\n";
		ret += super.toString();
		
		ret += "Length: " + mHandshakeMessages.size() + "\n";
		for (HandshakeMessage m : mHandshakeMessages) {
			ret += m.toString();
		}
		
		return ret;
	}
	
	public void convertToEncrypted()
	{
		ArrayList<HandshakeMessage> handshakeMessages = new ArrayList<>();
		for (@SuppressWarnings("unused") HandshakeMessage m : mHandshakeMessages) {
			// TODO: keep a local version of the raw payload
			handshakeMessages.add(new EncryptedMessage());
		}
		mHandshakeMessages = handshakeMessages;
	}
}
