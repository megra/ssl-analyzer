package org.marguerie.ssl_scanner.SSLPacketParser.protocols;

import org.marguerie.ssl_scanner.SSLPacketParser.exception.InvalidSSLPacketException;

public class AlertProtocol extends ProtocolMessage {

	/**
	 * Constants.
	 */
	final static int LEVEL_NULL		= -1;
	final static int LEVEL_WARNING	= 1;
	final static int LEVEL_FATAL	= 2;
	
	final static int DESCRIPTION_NULL								= -1;
	final static int DESCRIPTION_CLOSE_NOTIFY						= 0;
	final static int DESCRIPTION_UNEXPECTED_MESSAGE					= 10;
	final static int DESCRIPTION_BAD_RECORD_MAC						= 20;
	final static int DESCRIPTION_DECRYPTION_FAILED					= 21;
	final static int DESCRIPTION_RECORD_OVERFLOW					= 22;
	final static int DESCRIPTION_DECOMPRESSION_FAILURE				= 30;
	final static int DESCRIPTION_HANDSHAKE_FAILURE					= 40;
	final static int DESCRIPTION_NO_CERTIFICATE						= 41;
	final static int DESCRIPTION_BAD_CERTIFICATE					= 42;
	final static int DESCRIPTION_UNSUPPORTED_CERTIFICATE			= 43;
	final static int DESCRIPTION_CERTIFICATE_REVOKED				= 44;
	final static int DESCRIPTION_CERTIFICATE_EXPIRED				= 45;
	final static int DESCRIPTION_CERTIFICATE_UNKNOWN				= 46;
	final static int DESCRIPTION_ILLEGAL_PARAMETER					= 47;
	final static int DESCRIPTION_UNKNOWN_CA							= 48;
	final static int DESCRIPTION_ACCESS_DENIED						= 49;
	final static int DESCRIPTION_DECODE_ERROR						= 50;
	final static int DESCRIPTION_DECRYPT_ERROR						= 51;
	final static int DESCRIPTION_EXPORT_RESTRICTION					= 60;
	final static int DESCRIPTION_PROTOCOL_VERSION					= 70;
	final static int DESCRIPTION_INSUFICIENT_SECURITY				= 71;
	final static int DESCRIPTION_INTERNAL_ERROR						= 80;
	final static int DESCRIPTION_USER_CANCELED						= 90;
	final static int DESCRIPTION_NO_RENEGOTIATION					= 100;
	final static int DESCRIPTION_UNSUPPORTED_EXTENSION				= 110;
	final static int DESCRIPTION_CERTIFICATE_UNOBTAINABLE			= 111;
	final static int DESCRIPTION_UNRECOGNISED_NAME					= 112;
	final static int DESCRIPTION_BAD_CERTIFICATE_STATUS_RESPONSE	= 113;
	final static int DESCRIPTION_BAD_CERTIFICATE_HASH_VALUE			= 114;
	final static int DESCRIPTION_UNKNOWN_PSK_IDENTITY				= 115;
	
	/**
	 * Internal state.
	 */
	private int mLevel			= LEVEL_NULL;
	private int mDescription	= -1;
	
	public void setLevel(int level) throws InvalidSSLPacketException {
//		if (level == LEVEL_FATAL || level == LEVEL_WARNING)
//		{
			mLevel = level;
//		}
//		else
//		{
//			throw new InvalidSSLPacketException("Incorrect alert level: " + level);
//		}
	}


	public void setDescription(int desc) throws InvalidSSLPacketException {
//		if (desc == DESCRIPTION_CLOSE_NOTIFY ||
//				desc == DESCRIPTION_UNEXPECTED_MESSAGE ||
//				desc == DESCRIPTION_BAD_RECORD_MAC ||
//				desc == DESCRIPTION_DECRYPTION_FAILED ||
//				desc == DESCRIPTION_RECORD_OVERFLOW ||
//				desc == DESCRIPTION_DECOMPRESSION_FAILURE ||
//				desc == DESCRIPTION_HANDSHAKE_FAILURE ||
//				desc == DESCRIPTION_NO_CERTIFICATE ||
//				desc == DESCRIPTION_BAD_CERTIFICATE ||
//				desc == DESCRIPTION_UNSUPPORTED_CERTIFICATE ||
//				desc == DESCRIPTION_CERTIFICATE_REVOKED ||
//				desc == DESCRIPTION_CERTIFICATE_EXPIRED ||
//				desc == DESCRIPTION_CERTIFICATE_UNKNOWN ||
//				desc == DESCRIPTION_ILLEGAL_PARAMETER ||
//				desc == DESCRIPTION_UNKNOWN_CA ||
//				desc == DESCRIPTION_ACCESS_DENIED ||
//				desc == DESCRIPTION_DECODE_ERROR ||
//				desc == DESCRIPTION_DECRYPT_ERROR ||
//				desc == DESCRIPTION_EXPORT_RESTRICTION ||
//				desc == DESCRIPTION_PROTOCOL_VERSION ||
//				desc == DESCRIPTION_INSUFICIENT_SECURITY ||
//				desc == DESCRIPTION_INTERNAL_ERROR ||
//				desc == DESCRIPTION_USER_CANCELED ||
//				desc == DESCRIPTION_NO_RENEGOTIATION ||
//				desc == DESCRIPTION_UNSUPPORTED_EXTENSION ||
//				desc == DESCRIPTION_CERTIFICATE_UNOBTAINABLE ||
//				desc == DESCRIPTION_UNRECOGNISED_NAME ||
//				desc == DESCRIPTION_BAD_CERTIFICATE_STATUS_RESPONSE ||
//				desc == DESCRIPTION_BAD_CERTIFICATE_HASH_VALUE ||
//				desc == DESCRIPTION_UNKNOWN_PSK_IDENTITY)
//		{
			mDescription = desc;
//		}
//		else
//		{
//			throw new InvalidSSLPacketException("Incorrect alert description: " + desc);
//		}
	}
	
	public int getLevel()
	{
		return mLevel;
	}
	
	public int getDescription()
	{
		return mDescription;
	}
	
	public String getLevelToString()
	{
		switch (mLevel)
		{
		case 1:
			return "warning";
		case 2:
			return "fatal";
		default:
			return "unknown";
		}
	}

	public String getDescriptionToString()
	{
		switch (mDescription)
		{
		case DESCRIPTION_CLOSE_NOTIFY:
			return "close notify";
		case DESCRIPTION_UNEXPECTED_MESSAGE:
			return "unexpected message";
		case DESCRIPTION_BAD_RECORD_MAC:
			return "bad record mac";
		case DESCRIPTION_DECRYPTION_FAILED:
			return "decryption failed";
		case DESCRIPTION_RECORD_OVERFLOW:
			return "record overflow";
		case DESCRIPTION_DECOMPRESSION_FAILURE:
			return "decompression failure";
		case DESCRIPTION_HANDSHAKE_FAILURE:
			return "handshake failure";
		case DESCRIPTION_NO_CERTIFICATE:
			return "no certficate";
		case DESCRIPTION_BAD_CERTIFICATE:
			return "bad certificate";
		case DESCRIPTION_UNSUPPORTED_CERTIFICATE:
			return "unsupported certificate";
		case DESCRIPTION_CERTIFICATE_REVOKED:
			return "certificate revoked";
		case DESCRIPTION_CERTIFICATE_UNKNOWN:
			return "certificate unknown";
		case DESCRIPTION_ILLEGAL_PARAMETER:
			return "illegal parameter";
		case DESCRIPTION_UNKNOWN_CA:
			return "unknown CA";
		case DESCRIPTION_ACCESS_DENIED:
			return "access denied";
		case DESCRIPTION_DECODE_ERROR:
			return "decode error";
		case DESCRIPTION_DECRYPT_ERROR:
			return "decrypt error";
		case DESCRIPTION_EXPORT_RESTRICTION:
			return "export restriction";
		case DESCRIPTION_PROTOCOL_VERSION:
			return "protocol version";
		case DESCRIPTION_INSUFICIENT_SECURITY:
			return "insuficient security";
		case DESCRIPTION_INTERNAL_ERROR:
			return "internal error";
		case DESCRIPTION_USER_CANCELED:
			return "user canceled";
		case DESCRIPTION_NO_RENEGOTIATION:
			return "no renegotiation";
		case DESCRIPTION_UNSUPPORTED_EXTENSION:
			return "unsupported extension";
		case DESCRIPTION_CERTIFICATE_UNOBTAINABLE:
			return "certificate unobtainable";
		case DESCRIPTION_UNRECOGNISED_NAME:
			return "unrecognised name";
		case DESCRIPTION_BAD_CERTIFICATE_STATUS_RESPONSE:
			return "bad certificate status response";
		case DESCRIPTION_BAD_CERTIFICATE_HASH_VALUE:
			return "bad certificate hash value";
		case DESCRIPTION_UNKNOWN_PSK_IDENTITY:
			return "unknown psk identity";
		default:
			return "unknown";
		}
	}
	
	public String toString()
	{
		String ret = "[AlertProtocolMessage]\n";
		ret += super.toString();

		ret += "Level: " + mLevel + " (" + getLevelToString() + ")\n";
		ret += "Description: " + mDescription + " (" + getDescriptionToString() + ")\n";
		
		return ret;
	}
}
