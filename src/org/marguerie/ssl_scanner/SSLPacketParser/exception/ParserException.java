package org.marguerie.ssl_scanner.SSLPacketParser.exception;

public class ParserException extends Exception {
	private static final long serialVersionUID = 3046687297409469286L;

	public ParserException(String msg) {
		super(msg);
	}
}
