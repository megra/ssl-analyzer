package org.marguerie.ssl_scanner.SSLPacketParser.exception;

public class InvalidSSLPacketException extends ParserException {
	private static final long serialVersionUID = 2976955372324629485L;
	
	public InvalidSSLPacketException(String msg) {
		super(msg);
	}
}
