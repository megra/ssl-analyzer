package org.marguerie.ssl_scanner.SSLPacketParser.exception;

public class MissingDataException extends ParserException {
	private static final long serialVersionUID = -4119526187024307439L;

	public MissingDataException(String msg) {
		super(msg);
	}

}
