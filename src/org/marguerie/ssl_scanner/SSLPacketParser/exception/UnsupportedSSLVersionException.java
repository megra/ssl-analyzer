package org.marguerie.ssl_scanner.SSLPacketParser.exception;

public class UnsupportedSSLVersionException extends ParserException {
	private static final long serialVersionUID = -6384987799685258882L;

	public UnsupportedSSLVersionException(String msg) {
		super(msg);
	}
}
