package org.marguerie.ssl_scanner.SSLPacketParser;


import java.util.ArrayList;

import org.jnetpcap.nio.JBuffer;
import org.marguerie.ssl_scanner.SSLPacketParser.exception.InvalidSSLPacketException;
import org.marguerie.ssl_scanner.SSLPacketParser.exception.MissingDataException;
import org.marguerie.ssl_scanner.SSLPacketParser.exception.ParserException;
import org.marguerie.ssl_scanner.SSLPacketParser.exception.UnsupportedSSLVersionException;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.AlertProtocol;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.ApplicationProtocol;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.ChangeCipherSpecProtocol;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.HandshakeProtocol;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.ProtocolMessage;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.handshake_messages.CertificateMessage;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.handshake_messages.CertificateRequestMessage;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.handshake_messages.CertificateVerifyMessage;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.handshake_messages.ClientHelloMessage;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.handshake_messages.ClientKeyExchangeMessage;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.handshake_messages.EncryptedMessage;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.handshake_messages.FinishedMessage;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.handshake_messages.HandshakeMessage;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.handshake_messages.HelloRequestMessage;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.handshake_messages.NewSessionTicketMessage;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.handshake_messages.ServerHelloDoneMessage;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.handshake_messages.ServerHelloMessage;
import org.marguerie.ssl_scanner.SSLPacketParser.protocols.handshake_messages.ServerKeyExchangeMessage;

public class SSLPacketParser {
	private ArrayList<ProtocolMessage> mSSLPackets = new ArrayList<>();
	private Integer mLastCorrectOffset = 0;
	
	public ArrayList<ProtocolMessage> getSSLPackets()
	{
		return mSSLPackets;
	}

	public int getLastCorrectOffset()
	{
		return mLastCorrectOffset;
	}
	
	public void parse(JBuffer raw, int offset) throws ParserException
	{
		// Debug: Dump packet
		// System.out.println(raw.toHexdump());

		// Parse messages
		mLastCorrectOffset = 0;
		while (offset < raw.size())
		{
			int lengthPayload = getProtocolMessageLength(raw, offset); // length of message data, excluding the 5-bytes header

			ProtocolMessage m = parseMessage(raw, offset);
			parseVersion(raw, offset, m);
			
			// add if the full packet has been retrieved
			if (lengthPayload + 5 <= raw.size()) {
				mSSLPackets.add(m);
				mLastCorrectOffset = offset;
			}

			offset += lengthPayload + 5;
		}
		
		if (offset != raw.size())
			throw new MissingDataException("Need to reassemble next packet!");
	}

	private ProtocolMessage parseMessage(JBuffer raw, int offset) throws ParserException {
		int type = getByteAsInt(raw, offset);
		int lengthPayload = getProtocolMessageLength(raw, offset); // length of message data, excluding the 5-bytes header
		
		switch (type)
		{
		case ProtocolMessage.CONTENTTYPE_ALERT:
			return parseProtocolAlert(raw, offset, lengthPayload);
		case ProtocolMessage.CONTENTTYPE_APPLICATION:
			return parseProtocolApplication(raw, offset, lengthPayload);
		case ProtocolMessage.CONTENTTYPE_CHANGECYPHER:
			return parseProtocolChangeCipher(raw, offset, lengthPayload);
		case ProtocolMessage.CONTENTTYPE_HANDSHAKE:
			return parseProtocolHandshake(raw, offset, lengthPayload);
		default:
			throw new InvalidSSLPacketException("Unsupported protocol type: " + type);
		}
	}

	/**
	 * Bound checked method to fetch a byte from the packet, as an unsigned int
	 * @param buffer buffer to extract the byte from 
	 * @param pos position
	 * @return the byte value, as an unsigned int
	 * @throws MissingDataException 
	 */
	private int getByteAsInt(JBuffer buffer, int pos) throws MissingDataException {
		if (pos >= buffer.size())
			throw new MissingDataException("Tried to get byte: " + pos + " / " + buffer.size());
		return buffer.getByte(pos) & 0xFF;
	}

	/**
	 * Bound checked method to fetch a byteArray from the packet
	 * @param buffer buffer to extract the byte from
	 * @param start start position
	 * @param end end position
	 * @return the byte array
	 * @throws MissingDataException 
	 */
	private byte[] getByteArray(JBuffer buffer, int start, int end) throws MissingDataException {
		if (start >= buffer.size() || end >= buffer.size() || end < start)
			throw new MissingDataException("Tried to get bytes: " + start + "-" + end + "/" + buffer.size());
		return buffer.getByteArray(start, end);
	}

	private ProtocolMessage parseProtocolChangeCipher(JBuffer raw, int offset, int length) throws UnsupportedSSLVersionException, MissingDataException {
		ChangeCipherSpecProtocol p = new ChangeCipherSpecProtocol();

		p.setCCS(getByteAsInt(raw, offset + 5));

		return p;
	}

	private ProtocolMessage parseProtocolApplication(JBuffer raw, int offset, int length) throws UnsupportedSSLVersionException, MissingDataException {
		ApplicationProtocol p = new ApplicationProtocol();

		p.setData(getByteArray(raw, offset + 5, length));

		return p;
	}

	private ProtocolMessage parseProtocolAlert(JBuffer raw, int offset, int length) throws UnsupportedSSLVersionException, InvalidSSLPacketException, MissingDataException {
		AlertProtocol p = new AlertProtocol();
		
		p.setLevel(getByteAsInt(raw, offset + 5));
		p.setDescription(getByteAsInt(raw, offset + 6));

		return p;
	}

	private ProtocolMessage parseProtocolHandshake(JBuffer raw, int offset, int lengthPayload) throws UnsupportedSSLVersionException, InvalidSSLPacketException, MissingDataException {
		HandshakeProtocol p = new HandshakeProtocol();

		parseProtocolHandshakeMessages(raw, offset + 5, lengthPayload, p);
		
		return p;
	}

	private void parseProtocolHandshakeMessages(JBuffer raw, int offset, int lengthChunk,
			HandshakeProtocol p) throws InvalidSSLPacketException, MissingDataException {
		int length = offset + lengthChunk;
		
		while (offset < length)
		{
			int type = getByteAsInt(raw, offset);
			int lengthPayload = getHandshakeMessageLength(raw, offset);
			
			HandshakeMessage m = null;
			switch (type)
			{
			case HandshakeMessage.TYPE_HELLO_REQUEST:
				m = parseProtocolHandshakeMessageHelloRequest(raw, offset, lengthPayload);
				break;
			case HandshakeMessage.TYPE_CLIENT_HELLO:
				m = parseProtocolHandshakeMessageClientHello(raw, offset, lengthPayload);
				break;
			case HandshakeMessage.TYPE_SERVER_HELLO:
				m = parseProtocolHandshakeMessageServerHello(raw, offset, lengthPayload);
				break;
			case HandshakeMessage.TYPE_NEW_SESSION_TICKET:
				m = parseProtocolHandshakeMessageNewSessionTicket(raw, offset, lengthPayload);
				break;
			case HandshakeMessage.TYPE_CERTIFICATE:
				m = parseProtocolHandshakeMessageCertificate(raw, offset, lengthPayload);
				break;
			case HandshakeMessage.TYPE_SERVER_KEY_EXCHANGE:
				m = parseProtocolHandshakeMessageServerKeyExchange(raw, offset, lengthPayload);
				break;
			case HandshakeMessage.TYPE_CERTIFICATE_REQUEST:
				m = parseProtocolHandshakeMessageCertificateRequest(raw, offset, lengthPayload);
				break;
			case HandshakeMessage.TYPE_SERVER_HELLO_DONE:
				m = parseProtocolHandshakeMessageServerHelloDone(raw, offset, lengthPayload);
				break;
			case HandshakeMessage.TYPE_CERTIFICATE_VERIFY:
				m = parseProtocolHandshakeMessageCertificateVerify(raw, offset, lengthPayload);
				break;
			case HandshakeMessage.TYPE_CLIENT_KEY_EXCHANGE:
				m = parseProtocolHandshakeMessageClientKeyExchange(raw, offset, lengthPayload);
				break;
			case HandshakeMessage.TYPE_FINISHED:
				m = parseProtocolHandshakeMessageFinished(raw, offset, lengthPayload);
				break;
			default:
				m = parseProtocolHandshakeMessageEncrypted(raw, offset, lengthPayload);
				break;
			}
			
			if (m != null)
				p.addMessage(m);
			
			offset += lengthPayload + 4;
		}
	}

	private HandshakeMessage parseProtocolHandshakeMessageFinished(JBuffer raw,
			int offset, int lengthPayload) {
		FinishedMessage p = new FinishedMessage ();

		// TODO Auto-generated method stub
		
		return p;
	}

	private HandshakeMessage parseProtocolHandshakeMessageEncrypted(JBuffer raw,
			int offset, int lengthPayload) {
		EncryptedMessage p = new EncryptedMessage ();

		// TODO Auto-generated method stub
		
		return p;
	}

	private HandshakeMessage parseProtocolHandshakeMessageClientKeyExchange(
			JBuffer raw, int offset, int lengthPayload) {
		ClientKeyExchangeMessage p = new ClientKeyExchangeMessage();

		// TODO Auto-generated method stub
		
		return p;
	}

	private HandshakeMessage parseProtocolHandshakeMessageCertificateVerify(
			JBuffer raw, int offset, int lengthPayload) {
		CertificateVerifyMessage p = new CertificateVerifyMessage();

		// TODO Auto-generated method stub
		
		return p;
	}

	private HandshakeMessage parseProtocolHandshakeMessageServerHelloDone(
			JBuffer raw, int offset, int lengthPayload) {
		ServerHelloDoneMessage p = new ServerHelloDoneMessage();

		// TODO Auto-generated method stub
		
		return p;
	}

	private HandshakeMessage parseProtocolHandshakeMessageCertificateRequest(
			JBuffer raw, int offset, int lengthPayload) {
		CertificateRequestMessage p = new CertificateRequestMessage();

		// TODO Auto-generated method stub
		
		return p;
	}

	private HandshakeMessage parseProtocolHandshakeMessageServerKeyExchange(
			JBuffer raw, int offset, int lengthPayload) {
		ServerKeyExchangeMessage p = new ServerKeyExchangeMessage();

		// TODO Auto-generated method stub
		
		return p;
	}

	private HandshakeMessage parseProtocolHandshakeMessageCertificate(
			JBuffer raw, int offset, int lengthPayload) {
		CertificateMessage p = new CertificateMessage();

		// TODO Auto-generated method stub
		
		return p;
	}

	private HandshakeMessage parseProtocolHandshakeMessageServerHello(
			JBuffer raw, int offset, int lengthPayload) {
		ServerHelloMessage p = new ServerHelloMessage();

		// TODO Auto-generated method stub
		
		return p;
	}

	private HandshakeMessage parseProtocolHandshakeMessageNewSessionTicket(
			JBuffer raw, int offset, int lengthPayload) {
		NewSessionTicketMessage p = new NewSessionTicketMessage();

		// TODO Auto-generated method stub
		
		return p;
	}
	
	private HandshakeMessage parseProtocolHandshakeMessageClientHello(
			JBuffer raw, int offset, int lengthPayload) {
		ClientHelloMessage p = new ClientHelloMessage();

		// TODO Auto-generated method stub
		
		return p;
	}

	private HandshakeMessage parseProtocolHandshakeMessageHelloRequest(
			JBuffer raw, int offset, int lengthPayload) {
		HelloRequestMessage p = new HelloRequestMessage();

		// TODO Auto-generated method stub
		
		return p;
	}

	private int getHandshakeMessageLength(JBuffer raw, int offset) throws MissingDataException {
		return (getByteAsInt(raw, offset + 1) * 256 * 256)
				+ (getByteAsInt(raw, offset + 2) * 256)
				+ getByteAsInt(raw, offset + 3);
	}

	private int getProtocolMessageLength(JBuffer raw, int offset) throws MissingDataException {
		return getByteAsInt(raw, offset + 3) * 256
				+ getByteAsInt(raw, offset + 4);
	}

	private void parseVersion(JBuffer raw, int offset, ProtocolMessage p) throws UnsupportedSSLVersionException, MissingDataException {
		p.setVersion(
				getByteAsInt(raw, offset + 1),
				getByteAsInt(raw, offset + 2));
	}
}
